﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Register
    {
        [Required(ErrorMessage ="Что-то тут не так")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Что-то тут не так")]
        [Compare("PasswordConfirm", ErrorMessage ="Пароль не такой")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Что-то тут не так")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Enum;

namespace WebApplication1.Models
{
    public class Player
    {
        private string name;
        private Roles role;
        private bool isReady = false;
        private int randomId;
        private int votes = 0;
        private bool isDead = false;

        public Player(string _name, Roles _role, bool _isReady,  int _id, int _votes, bool _isDead)
        {
            name = _name;
            role = _role;
            isReady = _isReady;
            isDead = _isDead;
            randomId = _id;
            votes = _votes;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }


        public Roles Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
            }
        }

        public bool IsReady
        {
            get
            {
                return isReady;
            }
            set
            {
                isReady = value;
            }
        }

        public int Votes
        {
            get
            {
                return votes;
            }
            set
            {
                votes = value;
            }
        }

        public int RandomId
        {
            get
            {
                return randomId;
            }
            set
            {
                randomId = value;
            }
        }

        public bool IsDead
        {
            get
            {
                return isDead;
            }
            set
            {
                isDead = value;
            }
        }
    }
}
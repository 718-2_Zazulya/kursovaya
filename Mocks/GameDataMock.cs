﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Interfaces;

namespace WebApplication1.Mocks
{
    public class GameDataMock: IGameData
    {
        private int GameId;

        public GameDataMock()
        {
            DataTable query = Database.SendQuery("Select * from [GameData]");
            GameId = query.Rows.Count;
            Database.SendQuery("Insert INTO [GameData] VALUES ('0', '0', '0', 'false', 'false', '0', 'false')");
        }

        public void AddData(string name, object data)
        {
            Database.SendQuery($"Update [GameData] set {name} = '{data}' where id = '{GameId}'");
        }

        public bool GetParameter(string name)
        {
            DataTable query = Database.SendQuery("Select * from [GameData]");

            int j = SearchCollumn(query, name);

            if (bool.TryParse(query.Rows[GameId].ItemArray[j].ToString(), out bool result))
                return result;
            else
                throw new Exception("Хм, хочу лагман...");
        }

        public string GetMainName()
        {
            DataTable query = Database.SendQuery("Select * from [GameData]");

            int j = SearchCollumn(query, "nameMainChel");

            return query.Rows[GameId].ItemArray[j].ToString();
        }

        private int SearchCollumn(DataTable table, string name)
        {
            int j = 0;
            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (table.Columns[i].ColumnName == name)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}

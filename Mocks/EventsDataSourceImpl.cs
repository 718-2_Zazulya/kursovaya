﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class EventsDataSourceImpl : EventsDataSource
    {
        public List<Event> GetEvents()
        {
            DataTable resultat = Database.SendQuery("SELECT * FROM[Event]");

            List<Event> events = new List<Event>();

            for (int i = 0; i < resultat.Rows.Count; i++)
            {
                double time = double.Parse(resultat.Rows[i].ItemArray[1].ToString());
                EventType type = (EventType)int.Parse(resultat.Rows[i].ItemArray[2].ToString());
                string about = resultat.Rows[i].ItemArray[3].ToString();
                Event currentEvent = new Event(type, time, about);
                events.Add(currentEvent);
            }
            return events;
        }
    }
}

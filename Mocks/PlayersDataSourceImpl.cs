﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class PlayersDataSourceImpl : PlayersDataSource
    {
        public List<Player> getPlayers()
        {
            DataTable resultat = Database.SendQuery("SELECT * FROM [Plaers]");

            List<Player> players = new List<Player>();

            for (int i = 0; i < resultat.Rows.Count; i++)
            {
                string name = resultat.Rows[i].ItemArray[1].ToString();
                Roles role = (Roles)int.Parse(resultat.Rows[i].ItemArray[2].ToString());
                bool isReady = bool.Parse(resultat.Rows[i].ItemArray[3].ToString());
                int randomId = int.Parse(resultat.Rows[i].ItemArray[4].ToString());
                int votes = int.Parse(resultat.Rows[i].ItemArray[5].ToString());
                bool isDead = bool.Parse(resultat.Rows[i].ItemArray[6].ToString());
                Player currentPlayer = new Player(name, role, isReady, randomId, votes, isDead);
                players.Add(currentPlayer);
            }

            return players;
        }
    }
}

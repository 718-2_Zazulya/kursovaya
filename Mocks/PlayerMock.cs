﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class PlayerMock: IPlayer
    {

        private PlayersDataSource _ds = new PlayersDataSourceImpl();

        public PlayerMock(PlayersDataSource ds)
        {
            _ds = ds;
        }

        public IEnumerable<Player> Players
        {
            get
            {
                return _ds.getPlayers();
            }
        }

        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public Roles Role { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        //поиск игрока по имени
        public Player FindPlayer(string name)
        {
            foreach (Player player in Players)
            {
                if (player.Name == name)
                {
                    return player;
                }
            }
            throw new Exception("Че?");
        }

        //готов ли игрок? 
        public Player PlayerReadiness(string name)
        {
            //Database.SendQuery($"UPDATE Plaers set isReady = 'True' WHERE Name = '{name}'");
            throw new Exception("Че?");
        }
        //выборка по ролям 
        public Player RolPlayer(Roles roles)
        {
            foreach (Player current in Players)
            {
                if (current.Role == roles)
                {
                    return current;
                }
            }
            throw new Exception("чего?");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class EventMock : IEvent
    {
        private EventsDataSource _ds = new EventsDataSourceImpl();

        public EventMock(EventsDataSource ds)
        {
            _ds = ds;
        }

        public IEnumerable<Event> Events
        {
            get
            {
                return _ds.GetEvents();
            }
        }

        public void AddEvent(EventType typeOfEvent, double time, string text)
        {
            //string i_love_my_predovatel = $"INSERT [Event] VALUES ('{time}', '{(int)typeOfEvent}', '{text}')";
            Database.SendQuery($"INSERT [Event] VALUES ('{time}', '{(int)typeOfEvent}', '{text}')");
            
        }

        //выборка событий
        public List<Event> GetEvents(string query = "SELECT * FROM [Event]")
        {
            DataTable resultat = Database.SendQuery(query);

            List<Event> events = new List<Event>();

            for (int i = 0; i < resultat.Rows.Count; i++)
            {
                double time = double.Parse(resultat.Rows[i].ItemArray[1].ToString());
                EventType type = (EventType)int.Parse(resultat.Rows[i].ItemArray[2].ToString());
                string about = resultat.Rows[i].ItemArray[3].ToString();
                Event currentEvent = new Event(type, time, about);
                events.Add(currentEvent);
            }
            return events;
        }

        //ищем последнее нетекстовое событие
        public Event GetLastEvent()
        { 
            for (int i = Events.Count() - 1; i >= 0; i--)
            {
                if (Events.ElementAt(i).Type != EventType.Текст)
                {
                    return Events.ElementAt(i);
                }
            }
            return null;
        }

        //
        public Event EventFind(EventType type)
        {
            foreach (Event current in Events)
            {
                if (current.Type == type)
                {
                    return current;
                }
            }
            return null;
        }

        public Event AddEvent(int time, EventType type, string text)
        {
            Database.SendQuery($"INSERT Event VALUES ('{time}', '{(int)type}', '{text}')");
            return new Event(type, time, text);
        }
    }
}

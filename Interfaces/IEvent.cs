﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Data.Interfaces
{
    public interface IEvent
    {
        public IEnumerable<Event> Events { get; }
        public void AddEvent(EventType typeOfEvent, double time, string text);

        public Event EventFind(EventType type);
        public List<Event> GetEvents(string query = "SELECT * FROM [Event]");
        public Event GetLastEvent();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public interface EventsDataSource
    {
        public List<Event> GetEvents();
    }
}

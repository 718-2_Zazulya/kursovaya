﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Data.Interfaces
{
    public interface IPlayer
    {
        string Name { get; set; }
        Roles Role { get; set; }

        IEnumerable<Player> Players { get; }

        //public List<Player> GetPlayers(string query = "SELECT * FROM [Plaers]");

        public Player FindPlayer(string name);
    }
}

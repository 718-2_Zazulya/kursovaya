﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Interfaces
{
    public interface IGameData
    {
        public bool GetParameter(string name);
        public string GetMainName();

        public void AddData(string name, object data);
    }
}

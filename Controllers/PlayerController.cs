﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Models;

namespace WebApplication1.Mocks
{
    public class PlayerController: Controller
    {
        private readonly IPlayer players;
        private readonly IEvent events;

        public PlayerController(IPlayer _players, IEvent _events)
        {
            players = _players;
            events = _events;
        }
        
        public ViewResult List()
        {
            IEnumerable<Player> list = players.Players;
            return View(list);
        }

        public IActionResult GetVote(string playerName)
        {
            string name = Request.Cookies["name"];
            bool Voted = bool.Parse(Request.Cookies["Voted"]);
            Player me = players.FindPlayer(name);
            Player player = players.FindPlayer(playerName);
            Event lastEvent = events.GetLastEvent();

            bool isDay = lastEvent.Type == EventType.Голосование_день;
            bool isNight = lastEvent.Type == EventType.Голосование_ночь;
            bool isMafia = me.Role == Roles.MAFIA;
            bool isDonMafia = me.Role == Roles.DON_MAFIA;

            if (isDay || (isNight && (isMafia || isDonMafia)))
            {
                if (!Voted && !me.IsDead && !player.IsDead)
                {
                    Database.SendQuery($"UPDATE Plaers set Votes = '{player.Votes + 1}' WHERE Name = '{playerName}'");
                    CookieOptions options = new CookieOptions();
                    Response.Cookies.Append("Voted", "True", options);
                }
            }
            return Redirect("list");
        }

        public ViewResult Game()
        {
            IEnumerable<Event> test = events.Events;
            var _players = players.Players;
            string name = Request.Cookies["name"];
            bool isPlayerBe = false;
            foreach (Player player in _players)
            {
                if (player.Name == name)
                {
                    isPlayerBe = true;
                    break;
                }
                else
                {
                    isPlayerBe = false;
                }
            }

            if (!isPlayerBe)
            {
                Random rnd = new Random();
                CookieOptions options = new CookieOptions();
                Response.Cookies.Append("Voted", "False", options);
                Database.SendQuery($"INSERT [Plaers] VALUES ('{name}', '{(int)Roles.PEOPLE}', 'False', '{rnd.Next(1000, 9999)}', '0', 'False')");
            }
            ViewData["event"] = events.EventFind(EventType.Старт);
            //DestroyAllInYourLife();
            return View(players.Players);
        }

        private async void DestroyAllInYourLife()
        {
            bool isGameEnded = false;
            while (!isGameEnded)
            {
                if (Request.Cookies["isGameEnded"] != "true")
                {
                    Task zadershka = Task.Delay(10 * 1000);
                    await zadershka;
                }
                else
                {
                    isGameEnded = true;
                }

            }
            Database.ClearTable("player");
            Database.ClearTable("events");
            Response.Cookies.Append("isGameEnded", "false");
            ReturnToIndex();
        }

        private ActionResult ReturnToIndex()
        {
            return Redirect("/Home/Index");
        }

        public IActionResult StartGame()
        {
            string name = Request.Cookies["name"];
            Database.SendQuery($"UPDATE Plaers set isReady = 'True' WHERE Name = '{name}'");
            bool isAllReady = true;
            var _players = players.Players;
            foreach (Player player in _players)
            {
                if (!player.IsReady)
                {
                    isAllReady = false;
                    break;
                }
                else
                {
                    isAllReady = true;
                }
            }

            if (isAllReady)
            {
                if (events.EventFind(EventType.Старт) == null)
                {
                    Database.SendQuery($"INSERT [Event] VALUES ('5', '{(int)EventType.Старт}', 'Game is started')");
                    DataTable currentPlayers = Database.SendQuery($"Select * from Plaers order by RandomId");
                    for (int i = 0; i < currentPlayers.Rows.Count; i++)
                    {
                        GenerateRoles(currentPlayers.Rows[i].ItemArray[1].ToString());
                    }
                }
            }

            return Redirect("game");
        }

        public ActionResult GetPlayer()
        {
            return PartialView(players.Players);
        }

        public void GenerateRoles(string name)
        {
            bool isGenerated = false;
            Random random = new Random();
            while (!isGenerated)
            {
                int randomNumber = random.Next(0, 10);
                (List<bool> mafiaIsBe, bool dommafiaIsBe, bool sherifIsBe) = CheckAllRoles();
                if (mafiaIsBe.Count < 2 && randomNumber < 3)
                {
                    SetRole(Roles.MAFIA, name);
                    isGenerated = true;
                }
                else if (!dommafiaIsBe && randomNumber >=3 && randomNumber < 6)
                {
                    SetRole(Roles.DON_MAFIA, name);
                    isGenerated = true;
                }
                else if (!sherifIsBe && randomNumber >= 6)
                {
                    SetRole(Roles.SHERIF, name);
                    isGenerated = true;
                }
                else if (dommafiaIsBe && sherifIsBe && mafiaIsBe.Count >= 2)
                {
                    isGenerated = true;
                }
            }
        }

        private void SetRole(Roles role, string name)
        {
            Database.SendQuery($"UPDATE Plaers set Rol ='{(int)role}' WHERE Name = '{name}'");
        }

        private (List<bool> mafiaIsBe, bool dommafiaIsBe, bool sherifIsBe) CheckAllRoles()
        {
            DataTable roles =  Database.SendQuery($"SELECT Rol From Plaers");
            //Player player = players.RolPlayers();
            List<bool> mafiaIsBe = new List<bool>();
            bool dommafiaIsBe = false;
            bool sherifIsBe = false;
            for (int i = 0; i < roles.Rows.Count; i++)
            {
                if (roles.Rows[i].ItemArray[0].ToString() == "0")
                {
                    mafiaIsBe.Add(true);
                }
                else if (roles.Rows[i].ItemArray[0].ToString() == "1")
                {
                    dommafiaIsBe = true;
                }
                else if (roles.Rows[i].ItemArray[0].ToString() == "2")
                {
                    sherifIsBe = true;
                }
            }
            return (mafiaIsBe, dommafiaIsBe, sherifIsBe);
        }
    }
}
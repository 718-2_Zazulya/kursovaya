﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Data.Interfaces;
using WebApplication1.Enum;
using WebApplication1.Interfaces;
using WebApplication1.Mocks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EventController : Controller
    {
        private readonly IEvent events;
        private readonly IPlayer players;
        private readonly IGameData game;
        private bool isGameEnded;
        private bool isGameStarted;
        private bool isMainGay;
        private bool isGeneratorEnabled;
        public EventController(IEvent _events, IPlayer _player, IGameData _game)
        {
            events = _events;
            players = _player;
            game = _game;
        }

        private void LoadResources()
        {
            string mainName = game.GetMainName();

            isGameEnded = game.GetParameter("isGameEnded");
            isGameStarted = game.GetParameter("isGameStarted");
            isMainGay = mainName == Request.Cookies["name"];
            isGeneratorEnabled = game.GetParameter("generatorIsEnabled");
        }

        public async void ItsGameBaby()
        {
            game.AddData("generatorIsEnabled", "true");
            while (!isGameEnded)
            {
                
                int eventCount = (int)events.GetLastEvent().Type;
                StartEvent(eventCount);

                Event lastEvent = events.GetLastEvent();
                double time = lastEvent.Time;

                Task zadershka = Task.Delay((int)time * 1000);
                await zadershka;
                
            }
            game.AddData("generatorIsEnabled", "false");

        }

        private bool CheckDead(Roles checkRole = Roles.Looser)
        {
            foreach (Player player in players.Players)
            {
                //проверка для всех ролей
                if (checkRole == Roles.Looser)
                {
                    if (!player.IsDead)
                    {
                        return false;
                    }
                }
                else if (checkRole == Roles.MAFIA)
                {
                    if (((player.Role == Roles.MAFIA) || (player.Role == Roles.DON_MAFIA)) && (!player.IsDead))
                    {
                        return false;
                    }
                }
                else if (checkRole == Roles.PEOPLE)
                {
                    if ((player.Role == Roles.PEOPLE) || (player.Role == Roles.SHERIF) && !player.IsDead)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void Kill()
        {
            Player rouge = CheckVotes();

            if (rouge.Votes != 0)
            {
                Database.SendQuery($"UPDATE Plaers set isDead = 'True' WHERE name = '{rouge.Name}'");
                events.AddEvent(EventType.Текст, 0, $"Player {rouge.Name} was punished!");
            }
            else
            {
                events.AddEvent(EventType.Текст, 0, $"Nobody was punished.");
            }

            if (CheckDead(Roles.PEOPLE))
                SetEnd("Are you winning, mafia?");
            else if (CheckDead(Roles.Looser))
                SetEnd("And no one was there");
            else if (CheckDead(Roles.MAFIA))
                SetEnd("Putin molodec");
        }
           
        private Player CheckVotes()
        {
            Player rouge = new Player("Putin", Roles.Looser, false, -1, 0, false);
            foreach (Player player in players.Players)
            {
                if (player.Votes > rouge.Votes)
                {
                    rouge = player;
                }
            }
            return rouge;
        }

        private void SetEnd(string text)
        {
            game.AddData("isGameEnded", "true");
            events.AddEvent(EventType.Окончание, 0, text);
        }

        public ActionResult EventsList()
        {
            if (players.Players.Count() == 1)
                game.AddData("nameMainChel", players.Players.Last().Name);
            //GenerateBools("isGameStarted");
            //GenerateBools("isGameEnded");
            //GenerateBools("isMainGay");
            LoadResources();

            if (isMainGay && !isGameEnded)
            {
                if (events.Events.Count() != 0 && !isGeneratorEnabled)
                    ItsGameBaby();
            }

            return View(events.Events);
        }

        // GET: EventController
        public void StartEvent(int count)
        {
            switch (count)
            {
                case 0:
                    SetNextEvent(2, 20, EventType.День, "Morning sun is waking up");
                    break;
                case 1:
                    SetNextEvent(2, 20, EventType.День, "Morning sun is waking up");
                    Kill();
                    break;
                case 2:
                    SetNextEvent(3, 20, EventType.Голосование_день, "Kill friend or die");
                    break;
                case 3:
                    SetNextEvent(4, 20, EventType.Ночь, "Good night sweet prince");
                    Kill();
                    break;
                case 4:
                    SetNextEvent(1, 20, EventType.Голосование_ночь, "Kill friend or die");
                    break;
            }
        }

        private void SetNextEvent(int index, int time, EventType type, string text)
        {
            if (events.GetLastEvent().Type != type)
                events.AddEvent(type, time, text);
            game.AddData("NextEvent", $"{index}");
        }

    }
}

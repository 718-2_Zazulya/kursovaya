﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Enum;

namespace WebApplication1.Controllers
{
    public class ChatController : Controller
    {
        [HttpPost]
        public IActionResult AddMessage(string text)
        {
            string name = Request.Cookies["name"];
            Database.SendQuery($"INSERT [Event] VALUES ('0', '{(int)EventType.Текст}', '[{name}]:{text}')");
            return Redirect("~/Player/Game");
        }
    }
}

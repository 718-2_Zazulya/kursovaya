﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(string username, string password, string ReturnUrl)
        {
            //Database.Debug(ConfigurationManager.ConnectionStrings["database"].ToString(), Response);
            if (CheckData(username, 1) && CheckData(password, 2))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, username)
                };

                var claimsIdentity = new ClaimsIdentity(claims, "Login");
                CookieOptions options = new CookieOptions();
                Response.Cookies.Append("name", username, options);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                return Redirect(ReturnUrl == null ? "/Home/Index" : ReturnUrl);
            }
            else
            {
                return View();
            }
        }

        private bool CheckData(string data, int count)
        {
            DataTable users = Database.SendQuery("SELECT * FROM [users]");
            for (int i = 0; i < users.Rows.Count; i++)
            {
                string currentUser = users.Rows[i].ItemArray[count].ToString();
                if (data == currentUser)
                {
                    return true;
                }
            }
            return false;
        }

        public ViewResult Histori()
        {
            return View();
        }
        public ViewResult Roli()
        {
            return View();
        }

        public ViewResult Pravila()
        {
            return View();
        }

        public ViewResult Provil()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            Response.Cookies.Append("isGameStarted", "false");
            Response.Cookies.Append("isGameEnded", "false");
            Response.Cookies.Append("isMainGay", "false");
            Response.Cookies.Append("generatorIsEnabled", "false");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

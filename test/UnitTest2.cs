using NUnit.Framework;
using WebApplication1;
using Moq;
using WebApplication1.Mocks;
using WebApplication1.Models;
using System.Collections.Generic;
using WebApplication1.Enum;
using WebApplication1.Data.Interfaces;

namespace WebApplication1
{
    public class Tests1
    {

        private Mock<PlayersDataSource> ds;
        private PlayerMock playerMock;


        private List<Player> players = new List<Player>();

        [SetUp]
        public void Setup()
        {
            ds = new Mock<PlayersDataSource>();
            playerMock = new PlayerMock(ds.Object);

            players.Add(new Player("Nastya", Roles.PEOPLE, true, 1243, 12, false));
            players.Add(new Player("Nastya1", Roles.Looser, true, 1623, 12, false));
            players.Add(new Player("Nastya2", Roles.PEOPLE, true, 1123, 12, false));
        }

        [Test]
        public void Test2()  //�������� ��� ��� ������ ����� getPlayers � ���������
        {
            Player actual = new Player("Max", Roles.SHERIF, true, 1221313, 12, false);
            players.Add(actual);

            ds.Setup(p => p.getPlayers()).Returns(players);

            playerMock.RolPlayer(actual.Role);

            ds.Verify(p => p.getPlayers(), Times.Once);
        }
    }
}